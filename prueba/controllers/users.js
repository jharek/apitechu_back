var express = require('express');
var user_file = require('../user.json');
var bodyParser = require('body-parser')
var app = express();

const URL_BASE = '/techu-peru/v1/';
const gobal = require('../global.js');

app.use(bodyParser.json());

//GET users
//app.get(URL_BASE+'users/:id',

 function getUsers(req, res){
   let pos = req.params.id-1;
   //let  respuesta = user_file[pos];
   let respuesta = (user_file[pos] == undefined) ? {"msg":"usuario no encontrado"} : user_file[pos];
   /*if(respuesta){
     res.send(respuesta);
   }else{
     res.send({"msg":"usuario no encontrado"});
   }*/
   res.send(respuesta);
 };

//);



//POST users
//app.post(URL_BASE + 'users',
  function createUsers(req, res){
    console.log('POST de users JHAREK 2');

    let newID = user_file.length + 1;
    let newUser = {
       "id" : newID,
       "first_name" : req.body.first_name,
       "last_name" : req.body.last_name,
       "email" : req.body.email,
      "password" : req.body.password
    };
    user_file.push(newUser);
    console.log("nuevo usuario"  + newUser);
    res.send(newUser);
  }
  //);

  //PUT users
  //app.put(URL_BASE + 'users/:id',
    function updateUsers(req, res){
      console.log('PUT de users');
      let newUser = {
         "id" : req.params.id,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email" : req.body.email,
        "password" : req.body.password
      };
      user_file[req.params.id -1] = newUser;
      console.log("usuario actualizado" + newUser);
      res.send(newUser);
    }
  //);

  //DELETE users
  //app.delete(URL_BASE + 'users/:id',
    function deleteUsers(req, res){
      console.log('Delete de users.');
      let pos = req.params.id;
      user_file.splice(pos - 1, 1);

      res.send({"msg":"Usuario eliminado"});

    }
  //);

//POST Para LOGUEO CON Persistencia
//Login
app.post(URL_BASE + 'login',
  function(req, res){
    console.log('Login');
    console.log(req.body.email);
    console.log(req.body.password);

    let tam = user_file.length;
    let i = 0;
    let encontrado = false;

    while((i< user_file.length) && !encontrado){
        if(user_file[i].email == req.body.email && user_file[i].password == req.body.password ){
          encontrado = true;
          user_file[i].logged = true;
        }
        i++;
    }
    if(encontrado){
      writeUserDataToFile(user_file);
      res.send({"Encontrado0":"si","id":i})
    }
    else {
      res.send({"Encontrado":"no"})
    }

});



//POST Para LOG-OUT
//LOG-OUT
app.post(URL_BASE + 'logout/:id',
  function(req, res){
    console.log('LOG-OUT');
    console.log(req.body.email);
    console.log(req.body.password);

    let tam = user_file.length;
    let i = 0;
    let encontrado = false;

    while((i< user_file.length) && !encontrado){
        if(user_file[i].logged != undefined && user_file[i].id == req.params.id && user_file[i].email == req.body.email
          && user_file[i].password == req.body.password ){
          encontrado = true;
          //user_file[i].logged = true;
          delete user_file[i].logged;
        }
        i++;
    }
    if(encontrado)
      res.send({"Encontrado0":"logout correcto","id":i})
    else {
      res.send({"Encontrado":"logout incorrecto"})
    }
});

// POST a /apitechu/v1/logout/:id
// Tanto para logout correcto como incorrecto, devolver un mensaje informativo.
// En caso de logout correcto, guardar el cambio en el archivo.
// En el logout tenemos que quitar el campo logged del usuario (delete user.logged)


//LOGUEADOS
app.get(URL_BASE+'logueados',
 function (req, res){
   var logueados = [];
   let tam = user_file.length;
   let i = 0;

   while((i< user_file.length)){
       if(user_file[i].logged != undefined){
          logueados.push(user_file[i]);
       }
       i++;
   }

   res.send(logueados);
 }
);


//2. Mediante una Query String limitar el numero de usuarios a mostrar.
//LOGUEADOS
// app.get(URL_BASE+'logueados/:id',
//  function (req, res){
//    var logueados = [];
//    let tam = user_file.length;
//    let i = 0;
//
//    while((i< user_file.length)){
//        if(user_file[i].logged != undefined){
//           logueados.push(user_file[i]);
//        }
//        i++;
//    }
//
//    res.send(logueados);
//  }
// );

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

//Exports para que las funciones puedan ser accesibles en server.js
module.exports.getUsers = getUsers;
module.exports.createUsers = createUsers;
module.exports.updateUsers = updateUsers;
module.exports.deleteUsers = deleteUsers;
