var express = require('express');
//var user_file = require('./user.json');
var user_file = require('./data/user.json');
var bodyParser = require('body-parser')
var app = express();
var port = process.env.PORT || 3026;
const URL_BASE = '/techu-peru/v1/';

require('dotenv').config();
const apikeyMLab = "apiKey="+process.env.MLAB_API_KEY;
//const apikeyMLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';


const URL_BASE_MLAB = 'https://api.mlab.com/api/1/databases/techu52db/collections/';

var requestJSON = require('request-json');

const gobal = require('./global.js');

//Llamada a controlador de Users.
const userController = require('./controllers/users.js');
//const account_controller = require('./controllers/account_controller');

app.use(bodyParser.json());

//******************************************* Operaciones con JSON *******************************************
//La lògica està en la controlador "users.js"/ Se comenta el codigo para utilizar los de abajo con MLap
//GET
//app.get(URL_BASE + 'users/:id', userController.getUsers);

//POST users
//app.post(URL_BASE + 'users', userController.createUsers);

//PUT users
//app.put(URL_BASE + 'users/:id', userController.updateUsers);

//DELETE users
//app.post(URL_BASE + 'users/:id', userController.deleteUsers);


//******************************************* Operaciones con MONGO - MLab *******************************************

// GET users consumiendo API REST de mLab
app.get(URL_BASE + 'users',
 function(req, res) {
   console.log("GET /colapi/v3/users");
   var httpClient = requestJSON.createClient(URL_BASE_MLAB);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   httpClient.get('user?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

// GET user con ID consumiendo API REST de mLab
app.get(URL_BASE + 'users/:id',
 function(req, res) {
   console.log("GET /colapi/v3/users");
   var httpClient = requestJSON.createClient(URL_BASE_MLAB);
   var id= req.params.id;
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'q={"id":' + id + '}&';
   var queryStrField = 'f={"_id":0}&';
      console.log("PASO 1");
   httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});


// GET user con ID accounts consumiendo API REST de mLab
app.get(URL_BASE + 'users/:id/accounts',
 function(req, res) {
   console.log("GET /colapi/v3/users");
   console.log("req.params.id: " + req.params.id);
   var httpClient = requestJSON.createClient(URL_BASE_MLAB);
   var id= req.params.id;
   console.log("Cliente HTTP mLab creado.");
   var queryStringId = 'q={"id":' + id + '}&';
   var queryString = 'f={"_id":0}&';
   console.log("PASO 1");
   httpClient.get('account?' + queryStringId + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
       console.log("error " + err);
       console.log("respuestaMLab " + respuestaMLab);
       console.log("body " + body);
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

// POST users mLab - Crear un Usuario
app.post(URL_BASE + 'users',
 function(req, res) {

   var clienteMlab = requestJSON.createClient(URL_BASE_MLAB)
   console.log(req.body);

   clienteMlab.get('user?' + apikeyMLab,
     function(error, respuestaMLab, body) {
       newID = body.length + 1;
       console.log("newID" + newID);
       var newUser = {
         "id" : newID,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email_name" : req.body.email,
         "password_name" : req.body.password
       };

       clienteMlab.post(URL_BASE_MLAB + "user?" + apikeyMLab, newUser,
           function(error, respuestaMLab, body){
             console.log(body);
             res.status(201);
             res.send(body);
           });
     });
});


// PUT users mLab - Actualizar con ID
app.put(URL_BASE + 'users/:id',
function(req, res) {
 var id = req.params.id;
 var queryStringID = 'q={"id":' + id + '}&';
 var clienteMlab = requestJSON.createClient(URL_BASE_MLAB);
 console.log("id:" + id);

 clienteMlab.get('user?'+ queryStringID + apikeyMLab,
   function(error, respuestaMLab, body) {
      console.log("req.body" + req.body);
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';

    console.log(req.body);
    console.log(cambio);
    clienteMlab.put(URL_BASE_MLAB +'user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
     function(error, respuestaMLab, body) {
       console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
      //res.status(200).send(body);
      res.send(body);
     });
   });
});


// Petición PUT con id de mLab (_id.$oid)
 app.put(URL_BASE + 'usersmLab/:id',
   function (req, res) {
     var id = req.params.id;
     let userBody = req.body;
     var queryString = 'q={"id":' + id + '}&';
     var httpClient = requestJSON.createClient(URL_BASE_MLAB);
     httpClient.get('user?' + queryString + apikeyMLab,
       function(err, respuestaMLab, body){
         let response = body[0];
         console.log(body);
         //Actualizo campos del usuario
         let updatedUser = {
           "id" : req.body.id,
           "first_name" : req.body.first_name,
           "last_name" : req.body.last_name,
           "email" : req.body.email,
           "password" : req.body.password
         };//Otra forma simplificada (para muchas propiedades)
         // var updatedUser = {};
         // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
         // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
         // PUT a mLab
         httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
         //httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
           function(err, respuestaMLab, body){
             var response = {};
             if(err) {
                 response = {
                   "msg" : "Error actualizando usuario."
                 }
                 res.status(500);
             } else {
               if(body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "Usuario actualizado correctamente."
                 }
                 res.status(200);
               }
             }
             res.send(response);
           });
       });
 });


//DELETE user with id
app.delete(URL_BASE + "users/:id",
 function(req, res){
   var id = req.params.id;
   var queryStringID = 'q={"id":' + id + '}&';
   console.log(URL_BASE_MLAB + 'user?' + queryStringID + apikeyMLab);
   var httpClient = requestJSON.createClient(URL_BASE_MLAB);
   httpClient.get('user?' +  queryStringID + apikeyMLab,
     function(error, respuestaMLab, body){
       var respuesta = body[0];
       httpClient.delete(URL_BASE_MLAB + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
         function(error, respuestaMLab,body){
           res.send(body);
       });
     });
 });

//LOGIN con MLAB
 app.post(URL_BASE + 'login',
   function (req, res) {
      let email = req.body.email;
      let pass = req.body.password;
      var queryString = `q={"email": "${email}", "password": "${pass}"}&`;
      let limFilter = "l=1&";
      console.log(queryString);
      var httpClient = requestJSON.createClient(URL_BASE_MLAB);
      httpClient.get('user?'+queryString+limFilter+apikeyMLab,
        function(err, respuestaMLab, body){
          if(err){
            res.status(500);
            res.send(err);
          }else{
            if(body.length == 1){
              let login = '{"$set":{"logged":true}}';
                httpClient.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
                  function(errPut, resPut, bodyPut) {
                    if(errPut){
                      res.sebd(errPut);
                    }else{
                      res.send(bodyPut);
                    }
                  });
            }else{
               res.status(404);
               res.send({"msg":"Usuario no existe"});
            }
          }
        }
      );
 });


 //Login CON Persistencia / POST Para LOGUEO
 app.post(URL_BASE + 'login',
   function(req, res){
     console.log('Login');
     console.log(req.body.email);
     console.log(req.body.password);

     let tam = user_file.length;
     let i = 0;
     let encontrado = false;

     while((i< user_file.length) && !encontrado){
         if(user_file[i].email == req.body.email && user_file[i].password == req.body.password ){
           encontrado = true;
           user_file[i].logged = true;
         }
         i++;
     }
     if(encontrado){
       writeUserDataToFile(user_file);
       res.send({"Encontrado0":"si","id":i})
     }
     else {
       res.send({"Encontrado":"no"})
     }

 });



 // LOG-OUT / POST Para LOG-OUT
 app.post(URL_BASE + 'logout/:id',
   function(req, res){
     console.log('LOG-OUT');
     console.log(req.body.email);
     console.log(req.body.password);

     let tam = user_file.length;
     let i = 0;
     let encontrado = false;

     while((i< user_file.length) && !encontrado){
         if(user_file[i].logged != undefined && user_file[i].id == req.params.id && user_file[i].email == req.body.email
           && user_file[i].password == req.body.password ){
           encontrado = true;
           //user_file[i].logged = true;
           delete user_file[i].logged;
         }
         i++;
     }
     if(encontrado)
       res.send({"Encontrado0":"logout correcto","id":i})
     else {
       res.send({"Encontrado":"logout incorrecto"})
     }
 });



// //POST Para LOGUEO
// //Login
// app.post(URL_BASE + 'login',
//   function(req, res){
//     console.log('Login');
//     console.log(req.body.email);
//     console.log(req.body.password);
//
//     let tam = user_file.length;
//     let i = 0;
//     let encontrado = false;
//
//     while((i< user_file.length) && !encontrado){
//         if(user_file[i].email == req.body.email && user_file[i].password == req.body.password ){
//           encontrado = true;
//           user_file[i].logged = true;
//         }
//         i++;
//     }
//     if(encontrado)
//       res.send({"Encontrado0":"si","id":i})
//     else {
//       res.send({"Encontrado":"no"})
//     }
//
//     // var i;
//     // var idusuario;
//     // for (i = 0; i < user_file.length h; i++) {
//     //   if(user_file[i].email == req.body.email && user_file[i].password == req.body.password ){
//     //
//     //     idusuario = user_file[i].id
//     //     user_file[i].logged = true
//     //     break;
//     //   }
//     //
//     // }
//     //
//     // if(idusuario != 0 )
//     //   res.send({"Encontrado":"si","id":idusuario})
//     // else {
//     //   res.send({"Encontrado":"no"})
//     // }
//
// });



// POST a /apitechu/v1/logout/:id
// Tanto para logout correcto como incorrecto, devolver un mensaje informativo.
// En caso de logout correcto, guardar el cambio en el archivo.
// En el logout tenemos que quitar el campo logged del usuario (delete user.logged)


//LOGUEADOS
app.get(URL_BASE+'logueados',
 function (req, res){
   var logueados = [];
   let tam = user_file.length;
   let i = 0;

   while((i< user_file.length)){
       if(user_file[i].logged != undefined){
          logueados.push(user_file[i]);
       }
       i++;
   }

   res.send(logueados);
 }
);


//2. Mediante una Query String limitar el numero de usuarios a mostrar.
//LOGUEADOS
// app.get(URL_BASE+'logueados/:id',
//  function (req, res){
//    var logueados = [];
//    let tam = user_file.length;
//    let i = 0;
//
//    while((i< user_file.length)){
//        if(user_file[i].logged != undefined){
//           logueados.push(user_file[i]);
//        }
//        i++;
//    }
//
//    res.send(logueados);
//  }
// );

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

//Entregable de esta semana es subir al git el LOGIN y el LOGOUT

//METODO PRINCIPAL - LISTENER
app.listen(port, function () {
  console.log('Example app listening on port = ' + port);
});
